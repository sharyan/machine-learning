import copy
import math
import sys
import os
import csv
import argparse

__author__ = 'Shane Ryan 10340427'

__doc__ = """
Python implementation of the K Nearest Neighbours Algorithm

Class:  4BCT
Name:   Shane Ryan
ID:     10340427


Algorithm:  K-Nearest Neighbours Algorithm
Sources:
    - https://saravananthirumuruganathan.wordpress.com/2010/05/17/a-detailed-introduction-to-k-nearest-neighbor-knn-algorithm/
    - https://en.wikipedia.org/wiki/K-nearest_neighbor_algorithm
    - http://www.scholarpedia.org/article/K-nearest_neighbor
    - Course Slides




Design:
    - The data is obtained from reading csv files (which can contain categorical or numerical data, but not a mix of both)
    - The data file can be specified on the command line, if no file is specified the program finds and uses a csv file
    in the current directory
    - The data is then parses from the headers in the csv file
    - The data is then normalised using Z-Normalisation, using the mean and standard deviation of the training data
    contained in the csv file
    - The user is then asked for a prediction/new input which is then classified by the kNN Algorithm
    - The kNN algorithm can use Euclidean, Manhattan or Hamming distance measures, depending on the command line flag
    and/or the data type
    - The kNN algorithm then returns a classification for the input data/prediction using the average classification of
    the nearest k neighbours. For example, if there are 3 "yes" play classifications and 2 "no" classifications for 5
    nearest neighbours, then the returned classification will be "yes".


Testing:
    - Tests on the algorithm and the measures can be found in the test_knn.py (to run, enter "python test_knn.py", or
    if nose is installed, "nosetests test_knn")
    - Tests on the kNN algorithm use embedded test data that is skewed purposefully in the classification space

Run:
    - For help on running the program, type "python algorithm.py --help
"""





ignore_fields = ['Play', 'ID']


def parse_cli():
    parser = argparse.ArgumentParser(description='Python implementation of the K Nearest Neighbours Algorithm',
                                     epilog=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-k', '--k-number',
                        help='Number of neighbours to run the algorithm',
                        required=False,
                        nargs='?',
                        default=3,
                        dest='k',
                        metavar='neighbours')
    parser.add_argument('-f', '--file',
                        help="""The file containing the information to the classified.
                                (default: finds the first csv file in the directory and parses that)""",
                        required=False,
                        default="*",
                        nargs='?',
                        dest='file',
                        metavar='filepath')
    parser.add_argument('-c', '--categorical',
                        help="Sets the data type as categorical",
                        action="store_true",
                        required=False,
                        dest='categorical')
    parser.add_argument('-n', '--numerical',
                        help="Sets the data type as numerical",
                        action="store_true",
                        required=False,
                        dest='numerical')
    parser.add_argument('-m', '--measure',
                        help="""The distance measure to be used [euclid, manhattan, hamming]. If the data type
                                is categorical, the choice must be hamming, otherwise the choice must be either euclid
                                or manhattan. (default: euclid)""",
                        required=False,
                        default='euclid',
                        choices=['euclid', 'manhattan', 'hamming'],
                        nargs='?',
                        dest='measure')

    return vars(parser.parse_args())


def get_data_reader(data_file):
    if isinstance(data_file, file):
        return csv.DictReader(data_file)
    return None


def get_file(file_path):
    data = None
    if isinstance(file_path, basestring):
        if os.path.exists(file_path):
            data = open(file_path, "r")
        elif os.path.exists(os.path.abspath(file_path)):
            data = open(os.path.abspath(file_path), "r")
        else:
            files = [find_file for find_file in os.listdir(".") if os.path.isfile(os.path.join(".", find_file)) and find_file.endswith(".csv")]
            data = open(files[0])
        return data
    else:
        return data


def get_prediction(fields):
    prediction = {}
    print "Please enter a new entry\n"
    for field in fields:
        if field not in ignore_fields:
            input = raw_input("Value for %s: " % (str(field),))
            prediction[field] = input
    print "Making prediction for %s" % (prediction,)
    return prediction


def resize_vector(vector_a, vector_b):
    """
    Resize the vector so that each is comparable. The largest vector is reduced down
    to the common keys of the shorter vector. This is done by removing the vector
    attributes that are not common in each.
    """
    if len(vector_a) > len(vector_b):
        vector_a = {k: v for k, v in vector_a.items() if k in vector_b}
    else:
        vector_b = {k: v for k, v in vector_b.items() if k in vector_a}
    return vector_a, vector_b


def euclidean_distance(vector_a, vector_b):
    """
    Calculates the distance of vector_a to vector_b in euclidean space
    """
    vector_a, vector_b = resize_vector(vector_a, vector_b)
    summation = sum([math.pow(vector_a[field] - vector_b[field], 2) for field in vector_b])
    distance = math.sqrt(math.fabs(summation))
    return distance


def manhattan_distance(vector_a, vector_b):
    """
    Calculates the distance using manhattan distance measure
    """
    vector_a, vector_b = resize_vector(vector_a, vector_b)
    distance = sum([math.fabs(vector_b[field] - vector_a[field]) for field in vector_b])
    return distance


def hamming_distance(vector_a, vector_b):
    """
    If attribute from vector_a equals the value from vector_b the
    similarity score between the vectors is increased by 1. 0 Otherwise.
    """
    vector_a, vector_b = resize_vector(vector_a, vector_b)
    distance_score = 0
    for attribute in vector_a:
        if vector_a[attribute] == vector_b[attribute]:
            distance_score += 1
    return distance_score


def kNN(normalised_data, k, normalised_prediction, measure="euclid"):
    # calculate distances from the training data entries to the prediction
    distances = []
    if measure == "euclid":
        distances = [(entry, euclidean_distance(entry, normalised_prediction)) for entry in normalised_data]
    elif measure == "manhattan":
        distances = [(entry, manhattan_distance(entry, normalised_prediction)) for entry in normalised_data]
    elif measure == "hamming":
        distances = [(entry, hamming_distance(entry, normalised_prediction)) for entry in normalised_data]
    else:
        distances = [(entry, euclidean_distance(entry, normalised_prediction)) for entry in normalised_data]

    closest = []

    if not measure == "hamming":
        lowest = distances[0]
        for i in range(0, k):
            for value in distances:
                if math.fabs(value[1]) < math.fabs(lowest[1]):
                    lowest = value
            closest.append(lowest)
            distances.remove(lowest)
            lowest = distances[0]
    else:
        highest = distances[0]
        for i in range(0, k):
            for value in distances:
                if value[1] > highest[1]:
                    highest = value
            closest.append(highest)
            distances.remove(highest)
            highest = distances[0]

    # return the prediction
    yes = len([x for x in closest if x[0]["Play"] == 'yes'])
    no = len([x for x in closest if x[0]["Play"] == 'no'])

    if yes > no:
        return True
    else:
        return False


def z_normalisation(data, input_data, fieldnames):
    """
    Normalises the input data using the Z-Normalisation Technique.
    :param data: The data to be normalised
    :param means: The means of the columns or attributes of the data
    :param std_deviations: The standard deviations of the attribute values of the data
    :return: The normalised data set
    """
    # normalise the data
    means = {}
    std_deviations = {}
    for field in fieldnames:
        if field not in ignore_fields:
            values = []
            for entry in data:
                values.append(int(entry[field]))
            means[field] = mean(values)
            std_deviations[field] = stddev(values)

    if data:
        for entry in data:
            for field in entry:
                if field not in ignore_fields:
                    entry[field] = ((float(entry[field]) - means[field]) / std_deviations[field])
    if isinstance(input_data, list):
        for entry in input_data:
            for field in entry:
                if field not in ignore_fields:
                    entry[field] = ((float(entry[field]) - means[field]) / std_deviations[field])
    elif isinstance(input_data, dict):
        for field in input_data:
            if field not in ignore_fields:
                input_data[field] = ((float(input_data[field]) - means[field]) / std_deviations[field])

    return data, input_data


def mean(values):
    float_values = [float(x) for x in values]
    return sum(float_values) / len(float_values)


def stddev(values):
    float_values = [float(x) for x in values]
    variance = sum([x * x for x in float_values]) / len(values) - math.pow(mean(values), 2)
    return math.sqrt(variance)


def main():
    # parse the command line arguments
    args = parse_cli()

    is_categorical = args.pop("categorical", False)
    is_numerical = args.pop("numerical", False)
    distance_measure = args.pop("measure", "euclid")
    k = int(args.pop("k", 3))

    if is_categorical and is_numerical:
        print "Data type cannot be categorical and numerical, can only be one or the other in this implementation"
        sys.exit(-1)
    elif is_categorical and not is_numerical:
        print "Data is set to be categorical, hamming distance measure is to be used"
        distance_measure = "hamming"
    elif not is_categorical and is_numerical:
        print "Data is set to be numerical"
        if distance_measure == "manhattan":
            print "Using Manhattan distance measure"
        elif distance_measure == "euclid":
            print "Using Euclidean distance measure"
        else:
            print "Defaulting to using Euclidean distance measure"
            distance_measure == "euclid"
    else:
        print "Defaulting to numerical data as input"
        is_numerical = True

    # get the data from the file
    data_file = get_file(args.pop("file", None))

    reader = get_data_reader(data_file)

    training_data = []

    for entry in reader:
        training_data.append(entry)

    # get the prediction, or test entry from the user
    prediction = get_prediction(reader.fieldnames)

    # original copy of the input prediction to keep for later when displaying results
    original_prediction = copy.copy(prediction)

    # normalise the data, if not categorical
    if not is_categorical and is_numerical:
        data, prediction = z_normalisation(data=training_data, input_data=prediction, fieldnames=reader.fieldnames)

    # run the algorithm on the data
    play = kNN(data, k, prediction, measure=distance_measure)

    if play:
        print "The prediction is to play Tennis with %s" % (original_prediction,)
    else:
        print "The prediction is to not play Tennis with %s" % (original_prediction,)
        # output the results
    pass


if __name__ == "__main__":
    main()