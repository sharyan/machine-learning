from algorithm import euclidean_distance, hamming_distance, manhattan_distance, z_normalisation, kNN

__author__ = 'Shane Ryan 10340427'

import unittest


class TestDistanceMeasures(unittest.TestCase):
    def test_euclidean_distance_equal(self):
        # ID,Sun,Temp,Humidity,Wind,Play
        vector_a = {"Sun": 1, "Humidity": 1, "Temp": 1, "Wind": 1}
        self.assertEqual(euclidean_distance(vector_a, vector_a), 0.0)

    def test_euclidean_distance_not_equal(self):
        # ID,Sun,Temp,Humidity,Wind,Play
        vector_a = {"Sun": 1, "Humidity": 1, "Temp": 1, "Wind": 1}
        vector_b = {"Sun": 2, "Humidity": 2, "Temp": 2, "Wind": 2}
        self.assertEqual(euclidean_distance(vector_a, vector_b), 2)

    def test_euclidean_distance_not_equal_reverse(self):
        # ID,Sun,Temp,Humidity,Wind,Play
        vector_a = {"Sun": 1, "Humidity": 1, "Temp": 1, "Wind": 1}
        vector_b = {"Sun": 2, "Humidity": 2, "Temp": 2, "Wind": 2}
        self.assertEqual(euclidean_distance(vector_b, vector_a), 2)

    def test_hamming_distance_equal(self):
        vector_a = {"Sun": 'yes', "Humidity": 'high', "Temp": 'high', "Wind": 'low'}
        self.assertEqual(hamming_distance(vector_a, vector_a), 4)

    def test_hamming_distance_not_equal(self):
        vector_a = {"Sun": 'yes', "Humidity": 'high', "Temp": 'high', "Wind": 'low'}
        vector_b = {"Sun": 'no', "Humidity": 'low', "Temp": 'low', "Wind": 'high'}
        self.assertEqual(hamming_distance(vector_a, vector_b), 0)

    def test_hamming_distance_not_equal_reverse(self):
        vector_a = {"Sun": 'yes', "Humidity": 'high', "Temp": 'high', "Wind": 'low'}
        vector_b = {"Sun": 'no', "Humidity": 'low', "Temp": 'low', "Wind": 'high'}
        self.assertEqual(hamming_distance(vector_b, vector_a), 0)

    def test_manhattan_distance_equal(self):
        vector_a = {"Sun": 1, "Humidity": 1, "Temp": 1, "Wind": 1}
        self.assertEqual(manhattan_distance(vector_a, vector_a), 0.0)

    def test_manhattan_distance_not_equal(self):
        vector_a = {"Sun": 1, "Humidity": 1, "Temp": 1, "Wind": 1}
        vector_b = {"Sun": 2, "Humidity": 2, "Temp": 2, "Wind": 2}
        self.assertEqual(manhattan_distance(vector_a, vector_b), 4.0)

    def test_manhattan_distance_not_equal_reverse(self):
        vector_a = {"Sun": 1, "Humidity": 1, "Temp": 1, "Wind": 1}
        vector_b = {"Sun": 2, "Humidity": 2, "Temp": 2, "Wind": 2}
        self.assertEqual(manhattan_distance(vector_b, vector_a), 4.0)


class CommandLineTests(unittest.TestCase):
    def test_hamming_numeric(self):
        pass


class KNNTests(unittest.TestCase):
    def setUp(self):
        self.numerical_data = [{"Sun": 1, "Humidity": 20, "Temp": 10, "Wind": 1, "Play": "no"},
                               {"Sun": 2, "Humidity": 21, "Temp": 11, "Wind": 2, "Play": "no"},
                               {"Sun": 10, "Humidity": 100, "Temp": 50, "Wind": 10, "Play": "yes"},
                               {"Sun": 11, "Humidity": 110, "Temp": 55, "Wind": 12, "Play": "yes"}]

        self.categorical_data = [{"Sun": 'yes', "Humidity": "high", "Temp": "vhigh", "Wind": "high", "Play": "no"},
                                 {"Sun": 'yes', "Humidity": "vhigh", "Temp": "high", "Wind": "high", "Play": "no"},
                                 {"Sun": 'no', "Humidity": "low", "Temp": "vlow", "Wind": "low", "Play": "yes"},
                                 {"Sun": 'no', "Humidity": "vlow", "Temp": "low", "Wind": "low", "Play": "yes"}]

        # numerical values close to the two no classifications above
        self.numerical_no_prediction = {"Sun": 2, "Humidity": 30, "Temp": 9, "Wind": 3}

        # numerical values close to the two yes classifications above
        self.numerical_yes_prediction = {"Sun": 12, "Humidity": 105, "Temp": 60, "Wind": 11}

        z_normalisation(data=self.numerical_data,
                        input_data=self.numerical_no_prediction,
                        fieldnames=self.numerical_data[0].keys())
        z_normalisation(data=self.numerical_data,
                        input_data=self.numerical_yes_prediction,
                        fieldnames=self.numerical_data[0].keys())

    def test_knn_euclid_yes(self):
        result = kNN(self.numerical_data, 2, self.numerical_yes_prediction, measure="euclid")
        self.assertEqual(result, True)

    def test_knn_euclid_no(self):
        result = kNN(self.numerical_data, 2, self.numerical_no_prediction, measure="euclid")
        self.assertEqual(result, False)

    def test_knn_manhattan_yes(self):
        result = kNN(self.numerical_data, 2, self.numerical_yes_prediction, measure="manhattan")
        self.assertEqual(result, True)

    def test_knn_manhattan_no(self):
        result = kNN(self.numerical_data, 2, self.numerical_no_prediction, measure="manhattan")
        self.assertEqual(result, False)

    def test_knn_hamming_no(self):
        self.categorical_no_classification = {"Sun": 'yes', "Humidity": "high", "Temp": "high", "Wind": "high"}
        result = kNN(self.categorical_data, 2, self.categorical_no_classification, measure="hamming")
        self.assertEqual(result, False)

    def test_knn_hamming_yes(self):
        self.categorical_yes_classification = {"Sun": 'no', "Humidity": "low", "Temp": "low", "Wind": "low"}
        result = kNN(self.categorical_data, 2, self.categorical_yes_classification, measure="hamming")
        self.assertEqual(result, True)


unittest.main()


